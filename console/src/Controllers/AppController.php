<?php

namespace Console\Controllers;

/**
 * Class AppController
 *
 * @package Console\Controllers
 */
class AppController extends \Phycom\Console\Controllers\AppController
{
    public $languages = ['et','en'];

    public $systemEmail = 'info@phycom.local.com';

    public $vendorName = 'Phycom';
    public $vendorLegalName = 'Some company name';
    public $vendorRegNo = '14123455';
    public $vendorEmail = 'info@phycom.local.com';
    public $vendorPhone = '6372400';
    public $vendorPhoneCode = '372';

    public $vendorAddress = [
        'country' => 'EE',
        'province' => 'Harjumaa',
        'city' => 'Tallinn',
        'district' => 'Kesklinn',
        'street' => 'Tartu 000',
        'postcode' => '10344'
    ];
}
