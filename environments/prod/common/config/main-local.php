<?php
return [
    'components' => [
        'db' => [
            'enableQueryCache'  => true,
            'enableSchemaCache' => true
        ]
    ]
];
