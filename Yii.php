<?php

/**
 * This constant defines the application installation directory.
 */
defined('ROOT_PATH') or define('ROOT_PATH', dirname(__FILE__));
/**
 * This constant defines the phycom installation directory.
 */
defined('PHYCOM_PATH') or define('PHYCOM_PATH', ROOT_PATH . '/vendor/phycom');

require(ROOT_PATH . '/vendor/yiisoft/yii2/BaseYii.php');
require(ROOT_PATH . '/vendor/phycom/base/src-base/Models/Traits/ApplicationFactoryTrait.php');


use Phycom\Base\Models\Traits\ApplicationFactoryTrait;

/**
 * Yii bootstrap file.
 * Used for enhanced IDE code auto-completion.
 */
class Yii extends \yii\BaseYii
{
    /**
     * @var FrontendApplication|BackendApplication|WebApplication|ConsoleApplication the application instance
     */
    public static $app;
}

spl_autoload_register(['Yii', 'autoload'], true, true);
Yii::$classMap = require(__DIR__ . '/vendor/yiisoft/yii2/classes.php');
Yii::$container = new yii\di\Container();


/**
 * Class WebApplication
 * Include only Web application related components here
 *
 * @property-read \lysenkobv\GeoIP\GeoIP $geoip
 */
class WebApplication extends yii\web\Application
{
    use ApplicationFactoryTrait;
}

/**
 * Class FrontendApplication
 *
 * @property-read \alexandernst\devicedetect\DeviceDetect $deviceDetect
 *
 * @property-read \phycom\common\components\Cart $cart
 * @property-read \phycom\common\components\Cart $wishlist
 * @property-read \phycom\frontend\components\ModelFactory $modelFactory
 */
class FrontendApplication extends WebApplication
{

}

/**
 * Class BackendApplication
 *
 * @property-read \phycom\backend\components\ModelFactory $modelFactory
 */
class BackendApplication extends WebApplication
{

}

/**
 * Class ConsoleApplication
 * Include only Console application related components here
 */
class ConsoleApplication extends yii\console\Application
{
    use ApplicationFactoryTrait;
}
