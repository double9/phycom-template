<?php

return [
    'name' => 'Phycom',
    'shortName' => 'PC',
    'timeZone' => 'Europe/Tallinn',
    'language' => 'et',
    'components' => [
        'redis' => new \yii\helpers\UnsetArrayValue(),
        'session' => [
            'class' => \yii\web\Session::class,
            'redis' => new \yii\helpers\UnsetArrayValue(),
        ],
        'cache' => [
            'class' => yii\caching\FileCache::class,
            'keyPrefix' => APP . '_ecommerce',
        ],
        'db' => [
            'dsn'      => 'pgsql:host=' . PHYCOM_DB_HOST . ';port=' . PHYCOM_DB_PORT . ';dbname=' . PHYCOM_DB_NAME,
            'username' => PHYCOM_DB_USER,
            'password' => PHYCOM_DB_PASS,
        ],
        'mutex' => [
            'class' => \yii\mutex\PgsqlMutex::class,
        ],
        'urlManagerFrontend' => [
            'baseUrl' => '/',
            'hostInfo' => PHYCOM_URL_PROTOCOL . '//' . PHYCOM_HOST_PUBLIC . '/',
        ],
        'urlManagerBackend' => [
            'baseUrl' => '/',
            'hostInfo' => PHYCOM_URL_PROTOCOL . '//' . PHYCOM_HOST_ADMIN . '/',
        ],
        'country' => [
            'defaultCountry' => 'EE',
            'preferredCountries' => ['EE'],
        ],
        'lang' => [

        ],
        'pages' => [
            'items' => [
                'about'             => ['name' => 'About page'],
                'about-en'          => ['name' => 'About page EN'],
                'team',
                'testimonials',
                'terms'             => ['name' => 'Terms and conditions'],
                'privacy-policy'    => ['name' => 'Privacy policy'],
                'cookie-policy'     => ['name' => 'Cookie policy'],
                'contact',
                'events',
                'careers',
                'order-info',
            ]
        ],
        'urlManagerBackend' => require(__DIR__ . '/../../vendor/phycom/backend/config/url-manager.php'),
        'urlManagerFrontend' => [],
        // this is the high priority queue
        'queue1' => new \yii\helpers\ReplaceArrayValue([
            'class'    => \Phycom\Base\Components\Queue\Db::class,
            'channel'  => 'high-priority',
            'ttr'      => 5 * 60,   // Max time for anything job handling
            'attempts' => 3,        // Max number of attempts
        ]),
        // this is for low priority jobs
        'queue2' => new \yii\helpers\ReplaceArrayValue([
            'class'    => \Phycom\Base\Components\Queue\Db::class,
            'channel'  => 'default',
            'ttr'      => 5 * 60,   // Max time for anything job handling
            'attempts' => 3,        // Max number of attempts
        ]),
    ],
    'modules' => [
        'email' => [
            'modules' => [
                \Phycom\Base\Modules\Email\Module::PROVIDER_MAILER => [
                    'mailer' => [
                        'useFileTransport' => false,
                        'transport' => [
                            'host'       => PHYCOM_SMTP_HOST,
                            'port'       => PHYCOM_SMTP_PORT,
                            'username'   => PHYCOM_SMTP_USER,
                            'password'   => PHYCOM_SMTP_PASS,
                            'encryption' => 'ssl'
                        ],
                    ]
                ],
            ]
        ],
        'auth' => [
            'modules' => [
                \Phycom\Base\Modules\Auth\Module::METHOD_GOOGLE => [
                    'enabled' => true,
                ],
                \Phycom\Base\Modules\Auth\Module::METHOD_FACEBOOK => [
                    'enabled' => true,
                ]
            ],
        ]
    ]
];
