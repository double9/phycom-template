<?php

/**
 * Defines the application name
 */
defined('APP') or define('APP', ((bool)getenv('IS_DOCKER') ? getenv('APP_ID') : 'phycom-template'));
/**
 * Defines the environment prefix used for env variables
 */
defined('ENV') or define('ENV', ((bool)getenv('IS_DOCKER') ? 'APP' : strtoupper(APP)));

/**
 * By default main configuration is get from the environment
 * You can edit this as needed in real project.
 */
define( 'PHYCOM_DB_HOST', getenv(ENV . '_DB_HOST') );
define( 'PHYCOM_DB_PORT', getenv(ENV . '_DB_PORT') );
define( 'PHYCOM_DB_NAME', getenv(ENV . '_DB_NAME') );
define( 'PHYCOM_DB_USER', getenv(ENV . '_DB_USER') );
define( 'PHYCOM_DB_PASS', getenv(ENV . '_DB_PASS') );

define( 'PHYCOM_REDIS_HOST', getenv(ENV . '_REDIS_HOST') );
define( 'PHYCOM_REDIS_PORT', getenv(ENV . '_REDIS_PORT') );

define( 'PHYCOM_BEANSTALK_HOST', getenv(ENV . '_BEANSTALK_HOST') );
define( 'PHYCOM_BEANSTALK_PORT', getenv(ENV . '_BEANSTALK_PORT') );

define( 'PHYCOM_SMTP_HOST', getenv(ENV . '_SMTP_HOST') );
define( 'PHYCOM_SMTP_PORT', getenv(ENV . '_SMTP_PORT') );
define( 'PHYCOM_SMTP_USER', getenv(ENV . '_SMTP_USER') );
define( 'PHYCOM_SMTP_PASS', getenv(ENV . '_SMTP_PASS') );

define( 'PHYCOM_HOST_PUBLIC', getenv(ENV . '_HOST_PUBLIC') );
define( 'PHYCOM_HOST_ADMIN', getenv(ENV . '_HOST_ADMIN') );
define( 'PHYCOM_HOST_API', getenv(ENV . '_HOST_API') );

define( 'PHYCOM_URL_PROTOCOL', getenv(ENV . '_URL_PROTOCOL') );
