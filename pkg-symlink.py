#!/usr/bin/env python3

import json
import subprocess
import sys
import os

conf = 'symlink-packages.json'
if os.path.exists(conf):
    with open(conf) as packages:
        data = json.load(packages)
        for package in data:
            name = package.split('/')[1]
            print(name)
            # make a host symlink under dev-packages
            subprocess.call(["sudo rm -r $(PWD)/dev-packages/" + name], shell=True)
            subprocess.call(["sudo ln -s " + data[package] + " $(PWD)/dev-packages/" + name], shell=True)
            #subprocess.call(["sudo rm -r $(PWD)/vendor/" + package + " && sudo ln -s " + data[package] + " $(PWD)/vendor/" + package], shell=True)
        print(str(len(data)) + ' packages symlinked successfully')
else:
    print('package list "' + conf + '" was not found')

sys.exit(os.EX_OK)



