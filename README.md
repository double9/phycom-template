
## Phycom application template for building e-commerce site


### Ansible configuration for deployments

* Add a target host to `ansible/hosts` file

* Modify `ansible/group_vars/webservers/vars.yml` to suit your needs

* Add host-specific configuration under `ansible/host_vars`. It can be similar to `ansible/group_vars/webservers` but each host should have its subfolder named as host id

* Store all sensitive info encrypted by ansible vault




### Phycom package management

If you want to update phycom package with ALL dependencies there is a shorthand script for this.
The updater will run composer update inside docker container with --with-dependencies flag.

* To update all phycom packages execute `./pkg-update.py` with no arguments
* To update specific package execute `./pkg-update.py <package-name>` where package name is package you wan to update.


It is also possible to develop a package that is same time included with the project.
There is a script for this purpose that does not affect composer.json file. 
It means you can easily push your changes to repository without having to comment out local dev package symlink params in composer.json file
You can create a configuration file `symlink-packages.json` to root path. There you can define packages that will be symlinked.

Example:
```
{
    "double9/phycom": "/home/user/phycom"
}
```

Here the local directory `/home/user/phycom` will be symlinked to `vendor/double9/phycom`

To start the symlinking process execute `./pkg-symlink.py`
