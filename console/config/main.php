<?php

return [
    'controllerMap' => [
        'migrate' => [
            'migrationNamespaces' => [
                'yii\queue\db\migrations'
            ]
        ]
    ]
];
