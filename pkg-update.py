#!/usr/bin/env python3

import json
import subprocess
import sys
import os

# load list pf packages from the argument
packages = sys.argv[1:]

# try to load packages from symlink packages list
if not packages:
    conf = 'symlink-packages.json'
    if os.path.exists(conf):
        print('loading packages from ' + conf + '...')
        with open(conf) as symlinkPackageList:
            data = json.load(symlinkPackageList)
            for package in data:
                packages.append(package)

# try to load phycom packages from composer.json
if not packages:
    print('loading packages from composer.json...')
    with open('composer.json') as composer:
        data = json.load(composer)
        for package in data['require']:
            if package.startswith('double9/'):
                packages.append(package)

if packages:

    for package in packages:
        subprocess.call(["sudo rm -r $(PWD)/vendor/" + package + " || true"], shell=True)

    cmd = "docker-compose run --rm php composer update " + " ".join(packages) + " --with-dependencies --optimize-autoloader"

    # re-symlink packages if linked
    for package in packages:
        name = package.split('/')[1]

        if os.path.exists("dev-packages/" + name):
            cmd += " && sudo rm -r $(pwd)/vendor/" + package + " && sudo ln -s /dev-packages/" + name + " $(pwd)/vendor/" + package

    # execute the command
    print('executing update command: ' + cmd)
    subprocess.call([cmd], shell=True)

else:
    print('no packages found for update')

sys.exit(os.EX_OK)
